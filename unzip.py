import utils

RANGE = range(1, 115)

utils.command("mkdir unzip")

for x in RANGE:
	file = f"{str(x).zfill(3)}"
	utils.command(f"mkdir unzip/{file}")
	utils.command(f"unzip {file}.zip -d unzip/{file}")