import utils

URLS = "https://everyayah.com/data/Alafasy_128kbps/zips/"
RANGE = range(1, 115)

for x in RANGE:
	utils.command(f"wget {URLS + str(x).zfill(3)}.zip")